from distributed.protocol import keras
from keras.models import Sequential
from keras.layers import Dense
import json
import numpy as np

phonems = set()
with open('distinct_sounds.json') as words_file:
    data = json.load(words_file)
    for phonem in data:
        phonems.add(phonem)
phonems.add("None")
phonem_list = list(phonems)
phonem_list.sort()
print(phonems)
phonems_count = len(phonems)

training_data = np.load("training_set_clean_2048.npz")['arr_0']
x = training_data[:, phonems_count:]
y = training_data[:, 0:phonems_count]

print(x)
print(y)

model = Sequential()
model.add(Dense(80, input_dim=27, activation='relu'))
model.add(Dense(80, activation='relu'))
model.add(Dense(phonems_count, activation='softmax'))

model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])

model.fit(x, y, epochs=10, batch_size=60,validation_split=0.10, )
keras.callbacks.EarlyStopping(monitor='val_loss',
                              min_delta=0,
                              patience=2,
                              verbose=0, mode='auto')

model.save('model_after_cleanup_2048.h5')
