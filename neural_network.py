from keras.models import Sequential
from keras.layers import Dense
import json
import numpy as np

with open('sounds.json') as json_file:
    data = json.load(json_file)

    phonems = set()
    cases = 0
    for phonem in data:
        phonems.add(phonem)
        cases += len(data[phonem])
    phonem_list = list(phonems)
    phonem_list.sort()
    print(phonems)
    phonems_count = len(phonems)
    print("Cases:", cases)

    X = np.zeros((cases, 20))
    y = np.zeros((cases, phonems_count))
    index = 0
    for phonem in data:
        for v in data[phonem]:
            if len(v) == 20:
                X[index] = v
                y[index][phonem_list.index(phonem)] = 1
                index = index + 1
    print(X)
    print(y)

    model = Sequential()
    model.add(Dense(80, input_dim=20, activation='relu'))
    model.add(Dense(80, activation='relu'))
    model.add(Dense(phonems_count, activation='softmax'))

    model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])

    model.fit(X, y, epochs=50, batch_size=10)

    model.save('model.h5')
