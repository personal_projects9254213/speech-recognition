from sklearn.metrics import confusion_matrix
from keras.models import load_model
import json
import numpy as np
import sys
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

phonems = set()
with open('distinct_sounds.json') as words_file:
    data = json.load(words_file)
    for phonem in data:
        phonems.add(phonem)
phonems.add("None")
phonem_list = list(phonems)
phonem_list.sort()
print(phonem_list)
phonems_count = len(phonems)

#test = np.load("validation_set_clean.npz")

training_data = np.load("validation_set_clean_1024.npz")['arr_0']
print(training_data.shape)
x = training_data[:, phonems_count:]
y = training_data[:, 0:phonems_count]

print(x)
print(y)

model = load_model('model_after_cleanup_1024.h5')
prediction = model.predict(x)

print(y)
results = []
truth = []
for p in prediction:
    index = np.argmax(p)
    results.append(index)
for p in y:
    index = np.argmax(p)
    truth.append(index)
#results = np.array(results)
#truth = np.array(truth)
#print(results)
#print(truth)
#print(results.shape)
#print(truth.shape)
ph = np.zeros(phonems_count)
pt = np.zeros(phonems_count)
matrix = np.zeros((phonems_count, phonems_count))
for i in range(len(results)):
    r = results[i]
    t = truth[i]
    matrix[r, t] += 1
    ph[r] += 1
    pt[t] += 1
np.set_printoptions(threshold=sys.maxsize, suppress=True, linewidth=200)
print(matrix)

print(ph)
print(pt)
con_mat_norm = np.around(matrix.astype('float') / matrix.sum(axis=1)[:, np.newaxis], decimals=2)
 
con_mat_df = pd.DataFrame(con_mat_norm,
                     index = phonem_list, 
                     columns = phonem_list)

figure = plt.figure(figsize=(phonems_count, phonems_count))
sns.heatmap(con_mat_df, annot=True,cmap=plt.cm.Blues)
plt.tight_layout()
plt.ylabel('True label')
plt.xlabel('Predicted label')
plt.show()

#print(confusion_matrix(truth, prediction, labels=phonem_list))

