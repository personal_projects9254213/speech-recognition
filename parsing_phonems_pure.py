import xml.etree.ElementTree as ET
from os import listdir
from os.path import isfile, join, isdir
import os.path
from os import path
import librosa
import soundfile as sf
import json
import numpy as np

phonems = set()
with open('distinct_sounds.json') as words_file:
    data = json.load(words_file)
    for phonem in data:
        phonems.add(phonem)
phonems.add("None")
phonem_list = list(phonems)
phonem_list.sort()
print(phonems)
phonems_count = len(phonems)
samplerate = 0

mfcc_window_size = 512
mfcc_hop_size = 128

training_data = np.zeros((0, phonems_count + 27))
trash_data = np.zeros((0, phonems_count + 27))

articles = [f for f in listdir('english') if isdir(join('english', f))]

articles.sort()

article_index = 0
words = {}
sounds = []
soundCount = 0
distinct_words = {}

for article in articles:
    article_index += 1
    print("Found article ", article, " (", article_index, "/", len(articles), ")")
    article_path = join('english', article)
    filename = join(article_path, 'aligned.swc')
    
    if not path.exists(filename):
        continue
    root = ET.parse(filename).getroot()

    all_nodes = root.findall(".//ph")
    print("This article has", len(all_nodes), "phonems annotated")

    if len(all_nodes) == 0:
        continue

    recordings = []
    recording_offsets = []
    audio_files = [f for f in listdir(article_path) if isfile(join(article_path, f)) and f.endswith(".ogg")]
    audio_files.sort()
    try:
        for audio_file in audio_files:
            data, samplerate = librosa.load(join(article_path, audio_file))
            print("Sample rate", samplerate)
            print("Samples", len(data))
            print("Duration", len(data)/samplerate, "s")
            recording = {}
            recording['data'] = data
            recording['samplerate'] = samplerate
            recording['duration'] = len(data)/samplerate
            recordings.append(recording)

    except Exception as e:
        print("Cant read audio file", e)
        continue
    
    for audio in root.findall(".//prop[@key='DC.source.audio.offset']"):
        print("Audio offset", audio.attrib["value"], audio.attrib["group"])
        recording_offsets.append(float(audio.attrib["value"]))

    previous_starting = 0
    previous_ending = 0

    try:
        for ph in all_nodes:
            #sound = {}
            s = ph.attrib["type"]
            #sound['sound'] = s
            soundCount = soundCount + 1

            recording_index = 0
            for offset in recording_offsets:
                if float(ph.attrib['start']) > offset:
                    break
                recording_index = recording_index + 1
            #print("Offset", sound['start'], "is in recording", recording_index)
            start_in_file = float(ph.attrib['start']) - recording_offsets[recording_index]
            end_in_file = float(ph.attrib['end']) - recording_offsets[recording_index]
            starting_sample = int(start_in_file * recordings[recording_index]['samplerate']/1000)
            ending_sample = int(end_in_file * recordings[recording_index]['samplerate']/1000)

            sample = recordings[recording_index]['data'][starting_sample:ending_sample]
            mfcc = librosa.feature.mfcc(y=sample, sr=samplerate, n_fft=samplerate, hop_length=samplerate)
            S = np.abs(librosa.stft(sample, n_fft=samplerate, hop_length=samplerate))
            contrast = librosa.feature.spectral_contrast(S=S, n_fft=samplerate, hop_length=samplerate)

            phonem_index = phonem_list.index(s)
            output = np.zeros((phonems_count))
            output[phonem_index] = 1.0
            con = np.concatenate((output, mfcc.T[0].flatten(), contrast.T[0].flatten()))
            training_data = np.vstack((training_data, [con]))

            if previous_ending == starting_sample:
                trash_starting = int((previous_starting + previous_ending)/2)
                trash_ending = int((starting_sample + ending_sample)/2)
                
                sample = recordings[recording_index]['data'][trash_starting:trash_ending]
                mfcc = librosa.feature.mfcc(y=sample, sr=samplerate, n_fft=samplerate, hop_length=samplerate)
                S = np.abs(librosa.stft(sample, n_fft=samplerate, hop_length=samplerate))
                contrast = librosa.feature.spectral_contrast(S=S, n_fft=samplerate, hop_length=samplerate)

                phonem_index = phonem_list.index("None")
                output = np.zeros((phonems_count))
                output[phonem_index] = 1.0
                
                con = np.concatenate((output, mfcc.T[0].flatten(), contrast.T[0].flatten()))
                trash_data = np.vstack((training_data, [con]))

            previous_starting = starting_sample
            previous_ending = ending_sample
            
            #print(training_data.shape)
    except Exception as e:
        print(e)

    np.savez_compressed("training_data_pure.npz", training_data)
    np.savez_compressed("training_data_trash.npz", np.vstack((training_data, trash_data)))

print(training_data)

np.savez_compressed("training_data_pure.npz", training_data)
np.savez_compressed("training_data_trash.npz", np.vstack((training_data, trash_data)))

print("sounds ", soundCount)

