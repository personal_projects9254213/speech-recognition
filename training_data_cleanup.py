from keras.models import Sequential
from keras.layers import Dense
import json
import numpy as np

phonems = set()
with open('distinct_sounds.json') as words_file:
    data = json.load(words_file)
    for phonem in data:
        phonems.add(phonem)
phonems.add("None")
phonem_list = list(phonems)
phonem_list.sort()
print(phonems)
phonems_count = len(phonems)

training_data = np.load("training_data_2048.npz")['arr_0']
x = training_data[:, phonems_count:]
y = training_data[:, 0:phonems_count]

print(x)
print(y)

samples = {}

for i in range(len(y)):
    case = y[i]
    most = np.argmax(case)
    if case[most] > 0.7:
        ph = phonem_list[most]
        if ph not in samples:
            samples[ph] = []
        samples[ph].append((i, case[most]))

print("sorting")
for s in samples:
    print(s)
    samples[s] = sorted(samples[s], key=lambda x: -x[1])

result = np.zeros((0, phonems_count + 27))
print("Combining")
for i in range(2000):
    print("iteration", i)
    for ph in phonem_list:
        try:
            arr = samples[ph]
            sample_index = arr[i % len(arr)][0]
            result = np.vstack((result, training_data[sample_index,:]))
        except:
            "v"
np.savez_compressed("training_data_clean_2048.npz", result)

print(result)
print(result.shape)

