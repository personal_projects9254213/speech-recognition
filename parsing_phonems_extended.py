import xml.etree.ElementTree as ET
from os import listdir
from os.path import isfile, join, isdir
import os.path
from os import path
import librosa
import soundfile as sf
import json
import numpy as np

phonems = set()
with open('distinct_sounds.json') as words_file:
    data = json.load(words_file)
    for phonem in data:
        phonems.add(phonem)
phonems.add("None")
phonem_list = list(phonems)
phonem_list.sort()
print(phonems)
phonems_count = len(phonems)
samplerate = 0

mfcc_hop_size = 128

training_data = np.zeros((0, phonems_count + 27))

articles = [f for f in listdir('english') if isdir(join('english', f))]

articles.sort()

loaded_recordings = {}

for mfcc_window_size in range(512, 2561, 512):
    print("Started window size", mfcc_window_size)
    article_index = 0
    words = {}
    sounds = []
    soundCount = 0
    distinct_words = {}

    for article in articles:
        article_index += 1
        print("Found article ", article, " (", article_index, "/", len(articles), ")")
        article_path = join('english', article)
        filename = join(article_path, 'aligned.swc')
        
        if not path.exists(filename):
            continue
        root = ET.parse(filename).getroot()

        recordings = []
        recording_offsets = []
        audio_files = [f for f in listdir(article_path) if isfile(join(article_path, f)) and f.endswith(".ogg")]
        audio_files.sort()
        try:
            for audio_file in audio_files:
                audio_file_name = join(article_path, audio_file)
                if audio_file_name in loaded_recordings:
                    print("Using cached audio file")
                    data, samplerate = loaded_recordings[audio_file_name]
                else:
                    print("Loading audio file")
                    data, samplerate = librosa.load(audio_file_name)
                    loaded_recordings[audio_file_name] = (data, samplerate)
                print("Sample rate", samplerate)
                print("Samples", len(data))
                print("Duration", len(data)/samplerate, "s")
                recording = {}
                recording['data'] = data
                recording['samplerate'] = samplerate
                recording['duration'] = len(data)/samplerate
                mfcc = librosa.feature.mfcc(y=data, sr=samplerate, n_fft=mfcc_window_size, hop_length=mfcc_hop_size)
                S = np.abs(librosa.stft(data, n_fft=mfcc_window_size, hop_length=mfcc_hop_size))
                contrast = librosa.feature.spectral_contrast(S=S, n_fft=mfcc_window_size, hop_length=mfcc_hop_size)
                recording['features'] = np.vstack((mfcc, contrast)).T
                recordings.append(recording)

        except:
            print("Cant read audio file")
            continue
        
        for audio in root.findall(".//prop[@key='DC.source.audio.offset']"):
            print(audio.attrib["value"], audio.attrib["group"])
            recording_offsets.append(float(audio.attrib["value"]))

        all_nodes = root.findall(".//ph")
        print("This article has", len(all_nodes), "phonems annotated")
        for ph in all_nodes:
            
            sound = {}
            s = ph.attrib["type"]
            sound['sound'] = s
            soundCount = soundCount + 1

            recording_index = 0
            for offset in recording_offsets:
                if float(ph.attrib['start']) > offset:
                    break
                recording_index = recording_index + 1
            #print("Offset", sound['start'], "is in recording", recording_index)
            start_in_file = float(ph.attrib['start']) - recording_offsets[recording_index]
            end_in_file = float(ph.attrib['end']) - recording_offsets[recording_index]
            starting_sample = int(start_in_file * recordings[recording_index]['samplerate']/1000)
            ending_sample = int(end_in_file * recordings[recording_index]['samplerate']/1000)

            sound['start'] = starting_sample
            sound['end'] = ending_sample

            sounds.append(sound)

            #phonem_index = phonem_list.index(s)
            #output = np.zeros((phonems_count))
            #output[phonem_index] = 1.0
            #con = np.concatenate((output, features.flatten()))
            #training_data = np.vstack((training_data, [con]))
            
            #print(training_data.shape)

        sounds = sorted(sounds, key=lambda k: k['start']) 
        recording_index = 0
        for recording in recordings:
            features = recording['features']
            print("Recording offset is", recording_offsets[recording_index])
            feature_start = int(recording_offsets[recording_index] * recordings[recording_index]['samplerate'] / 1000)

            for i in range(len(features)):
                feature = features[i]
                if i % 10000 == 0:
                    print("Finished", i, "/", len(features))
                feature_end = feature_start + mfcc_window_size
                durations = np.zeros(phonems_count)
                durations_sum = 0
                #print("feature start", feature_start, "feature end", feature_end)
                for phonem in sounds:
                    if (feature_end > phonem['start'] and feature_start < phonem['start']) or (feature_end > phonem['end'] and feature_start < phonem['end']):
                        #print("In")
                        phonem_index = phonem_list.index(phonem['sound'])
                        new_duration = min(feature_end, phonem['end']) - max(feature_start, phonem['start'])
                        durations[phonem_index] += new_duration
                        durations_sum += new_duration
                        
                    if feature_start > phonem['end']:
                        break

                if durations_sum > 0:
                    if durations_sum < mfcc_window_size:
                        phonem_index = phonem_list.index("None")
                        durations[phonem_index] += mfcc_window_size - durations_sum
                    durations = durations / durations.sum()
                    #print(durations)
                    con = np.concatenate((durations, feature.flatten()))
                    training_data = np.vstack((training_data, [con]))

                sounds = [d for d in sounds if d['end'] > feature_start]
                if len(sounds) == 0:
                    break

                #sounds = filter(lambda x: feature_start > x['end'], sounds)
                feature_start += mfcc_hop_size
            recording_index = recording_index + 1
        np.savez_compressed("training_data_"+str(mfcc_window_size)+".npz", training_data)

    print(training_data)

    np.savez_compressed("training_data_"+str(mfcc_window_size)+".npz", training_data)

    print("sounds ", soundCount)

