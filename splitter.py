import random
from os import listdir
from os.path import isfile, join, isdir
import json
import numpy as np

def main():
    td = np.load('training_data_clean_2048.npz')['arr_0']
    print(td.shape)
    print(td[0].shape)
    print(td[1].shape)

    number_of_desired_samples_test = int(td.shape[0] * 0.2)
    print("test_set_length",number_of_desired_samples_test)
    indices = random.sample(range(td.shape[0]), number_of_desired_samples_test)
    counter = 0
    result = td[indices,:]
    training = td[list(set(range(td.shape[0])) - set(indices))]
    # for i in indices:
    #     counter = counter + 1
    #     print("(",counter,"/", len(indices), ")")
    #     result = np.vstack((result, td[i, :]))
    #     np.delete(td, i, 0)

    print(td.shape[0])
    print(result.shape[0])
    np.savez_compressed("training_set_clean_2048.npz", td)
    np.savez_compressed("validation_set_clean_2048.npz", td)



# phonems = set()
# with open('distinct_sounds.json') as words_file:
#     data = json.load(words_file)
#     for phonem in data:
#         phonems.add(phonem)
# phonems.add("None")
# phonem_list = list(phonems)
# phonem_list.sort()
# print(phonems)
# phonems_count = len(phonems)
#
# training_data = np.zeros((0, phonems_count + 20))
#
# files = [f for f in listdir('training_data') if isfile(join('training_data', f))]
# for file in files:
#     filename = join('training_data', file)
#     print("Adding file", filename)
#     td = np.load(filename)['arr_0']
#     training_data = np.vstack((training_data, td))
#
# np.savez_compressed("training_data.npz", training_data)



if __name__ == "__main__":
    main()