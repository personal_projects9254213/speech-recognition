import xml.etree.ElementTree as ET
from os import listdir
from os.path import isfile, join, isdir
import os.path
from os import path
import librosa
import soundfile as sf
import json
import numpy as np

words = {}
sounds = {}
soundCount = 0
distinct_sounds = {}
distinct_words = {}
durations = {}
samplerate = 0
#comment 1
#comment 2222
articles = [f for f in listdir('english') if isdir(join('english', f))]
for article in articles:
    print("Found article ", article)
    article_path = join('english', article)
    filename = join(article_path, 'aligned.swc')
    
    if not path.exists(filename):
        continue
    root = ET.parse(filename).getroot()

    recordings = []
    recording_offsets = []
    audio_files = [f for f in listdir(article_path) if isfile(join(article_path, f)) and f.endswith(".ogg")]
    audio_files.sort()
    try:
        for audio_file in audio_files:
            data, samplerate = librosa.load(join(article_path, audio_file), sr=None)
            print("Sample rate", samplerate)
            print("Samples", len(data))
            print("Duration", len(data)/samplerate, "s")
            recording = {}
            recording['data'] = data
            recording['samplerate'] = samplerate
            recording['duration'] = len(data)/samplerate
            recordings.append(recording)
    except:
        print("Cant read audio file")
        continue
    
    for audio in root.findall(".//prop[@key='DC.source.audio.offset']"):
        print(audio.attrib["value"], audio.attrib["group"])
        recording_offsets.append(float(audio.attrib["value"]))

    for n in root.findall(".//n"):
        word = {}
        w = n.attrib["pronunciation"]
            
        if w in distinct_words:
            distinct_words[w] = distinct_words[w] + 1
        else:
            distinct_words[w] = 1
        phonems = []
        for ph in n.findall("./ph"):
            sound = {}
            s = ph.attrib["type"]
            phonems.append(s)
            sound['sound'] = s

            recording_index = 0
            for offset in recording_offsets:
                if float(ph.attrib['start']) > offset:
                    break
                recording_index = recording_index + 1
            #print("Offset", sound['start'], "is in recording", recording_index)
            start_in_file = float(ph.attrib['start']) - recording_offsets[recording_index]
            end_in_file = float(ph.attrib['end']) - recording_offsets[recording_index]
            starting_sample = int(start_in_file * recordings[recording_index]['samplerate']/1000)
            ending_sample = int(end_in_file * recordings[recording_index]['samplerate']/1000)
            #print("start sample is", starting_sample, "and ending sample is", ending_sample)
            #print(len(recordings[recording_index]['data']))
            sample = recordings[recording_index]['data'][starting_sample:ending_sample]
            #fn = 'ph/'+s+'.wav'
            #sf.write(fn, sample, recordings[recording_index]['samplerate'], subtype='PCM_24')
            if not s in sounds:
                sounds[s] = []
            sounds[s].extend(sample)
        

class NumpyEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, np.ndarray):
            return obj.tolist()
        return json.JSONEncoder.default(self, obj)

for ph in sounds:
    sf.write('phonems/'+ph+'.wav', sounds[ph], samplerate, subtype='PCM_24')


