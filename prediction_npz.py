from keras.models import Sequential
from keras.layers import Dense
import json
import numpy as np
from keras.models import load_model
import librosa


def Reverse(lst):
    return [ele for ele in reversed(lst)]



def compact(recognized):
    compacted_recognized = []
    compacted_recognized.append(recognized[0])

    for i in range(1, len(recognized)):
        #print(recognized[i])
        if recognized[i] != recognized[i-1]:
            compacted_recognized.append(recognized[i])
    return compacted_recognized
'''
This function compresses the prediction buffer with the same criteria it takes to compress the 
obtained chain of accepted phonems, getting the likelyhood chain for the accepted length
'''
def prob_compression(preddiction_buffer,recognized):
    compacted_prediction = []
    prediction_sum = prediction_buffer[0]
    prob_num = 1
    for i in range(1, len(recognized)):
        if recognized[i] != recognized[i-1]:
            compacted_prediction.append(prediction_sum/prob_num)
            prediction_sum = preddiction_buffer[i]
            prob_num = 1
        else:
            prediction_sum += preddiction_buffer[i]
            ++prob_num
        if(i == len(recognized)-1):
            compacted_prediction.append(prediction_sum)
    return compacted_prediction

'''
This function receives an array with the last outputs of the neural networks
should be called every space so that it checks the possible words erasing those spaces 
and the likelyhood of each of those(applied with the original chain, substitutes HMM's correction
in case it has a modifying output)
IMPORTANT: the input must be a chain of not recognised phonems and spaces
'''
def space_prediction(chain):
    with open('words.json') as json_file:
        words = json.load(json_file)
        wordss = [[]]
        wordd = ""
        ind = 0
        for phon in chain:
            if phon != ' ':#substitute with the phonem representation of space
                wordss[ind].append(phon)
            else:
                ++ind
        for j in range(1,ind+1):#for each number of consecutive erased spaces
            for k in range(0,ind+1-j):
                for s in range(0,j):
                    wordd += wordss[k+s]
                for word in words:
                    if wordd in word:
                        #combination found
                        #giving chain the shape of its corresponding words now
                        chain = ""
                        for w in range(0,k):
                            chain += wordss[w]
                            chain += ' '
                        chain +=wordd
                        for w in range(k+s+1,ind+1):
                            chain += ' '
                            chain += wordss[w]
                        return
                wordd = ""

'''
levenshtein distance
'''
def lev_distance(chain1, chain2):
  d=dict()
  for i in range(len(chain1)+1):
     d[i]=dict()
     d[i][0]=i
  for i in range(len(chain2)+1):
     d[0][i] = i
  for i in range(1, len(chain1)+1):
     for j in range(1, len(chain2)+1):
        d[i][j] = min(d[i][j-1]+1, d[i-1][j]+1, d[i-1][j-1]+(not chain1[i-1] == chain2[j-1]))
  return d[len(chain1)][len(chain2)]
        
'''
array of equidstant words
'''
def lev_equi(chain,words):
    output = {}
    min = -1
    minn = -1
    for word in words:
        for pronounciation in words[word]:
            aux = lev_distance(pronounciation,chain)
            if(min == -1):
                min = aux
            else:
                if min > aux:
                    min = aux
        if(minn == -1):
            minn = min
        else:
            if minn > min:
                minn = min
        if not min in output:
            output[min] = []
        output[min].append(word)
        min = -1
    return output[minn]


'''
gets the most likely out of the equidistant words
'''
def word_corrector(prediction_buffer,recognized,phonem_list):
    with open('words.json') as json_file:
        index = 0
        index2 = 0
        words = json.load(json_file)
        arrayy = lev_equi(recognized,words)
        result = {}
        min = -1#minimum change weight of each word, the lower, the closer to the reality
        minn = -1#the minimum value we can output
        index = 0#position of the different phonem
        aux = 0
        print("arrayy",arrayy)
        for word in arrayy:
            print("word",word)
            for pronounciation in words[word]:
            
                for phonn in pronounciation:
                    if(index == len(recognized)):
                        break
                    if(phonn != recognized[index]):
                        
                        for i in [i for i,x in enumerate(phonem_list) if x == phonn]:
                            index2 = i
                        #MODIFY THIS LINE IF LEVANSTENS WEIGHTS ARE CHANGED OR ARGUMENTS ARE ADDED
                        aux += 1-prediction_buffer[index][0][index2]#this gets the probability of this substitution phonem and ads to the change weight calculus 1-this number
                    index += 1
                index = 0
                #MODIFY THIS LINE IF THE WEIGHT OF ADDING NEW PHONEMS IS CHANGED
                aux += 2*abs(len(pronounciation)-len(recognized))#we add 2 to the change weight for each phonem we add/remove
                if(min == -1):
                    min = aux
                else:
                    if min > aux:
                        min = aux
                aux = 0
            if(minn == -1):
                minn = min
            else:
                if minn > min:
                    minn = min
            if not min in result:
                result[min] = []
            result[min].append(word)
            min = -1
        print(minn)
        return result[minn]
def possibilities_to_variations(possibilities):
    result = []
    if len(possibilities) > 1:
        p = possibilities.pop(0)
        vars = possibilities_to_variations(possibilities)
        for phonem in p:
            for var in vars:
                r = [ phonem ]
                r.extend(var)
                result.append(r)
    elif len(possibilities) > 0:
        result = possibilities
    return result

def get_variations(predictions):
    window_size = 8
    rankings = 3
    possibilities = []
    for i in range(0, len(predictions)-window_size, window_size):
        freqs = np.zeros(phonems_count)
        for j in range(i, i+window_size):
            prediction = predictions[j]
            for k in range(rankings):
                x = int(np.argmax(prediction))
                freqs[x] += 1
                prediction[x] = 0
        possible_phonems = []
        for k in range(rankings):
            phonem = np.argmax(freqs)
            freqs[phonem] = 0
            possible_phonems.append(phonem)
        possibilities.append(possible_phonems)
    print(possibilities)
    return possibilities_to_variations(possibilities)
    
words = {}
phonems = set()
prediction_buffer = []
index2 = 0
with open('distinct_sounds.json') as words_file:
    data = json.load(words_file)
    for phonem in data:
        phonems.add(phonem)
with open('words.json') as words_file:
    data = json.load(words_file)
    for word in data:
        words[word] = data[word][0]
phonems.add("None")
phonem_list = list(phonems)
phonem_list.sort()
print(phonems)
phonems_count = len(phonems)

model = load_model('model_after_cleanup_512.h5')
data, samplerate = librosa.load('words/days.wav')
interval_ms = 50
interval_samples = int(interval_ms * samplerate / 1000)
print(interval_ms, "ms at ", samplerate, "samples/s equals to ", interval_samples, "samples")
features = librosa.feature.mfcc(y=data, sr=samplerate, hop_length = 64, n_fft=512)
S = np.abs(librosa.stft(data, n_fft=512, hop_length=64))
contrast = librosa.feature.spectral_contrast(S=S, n_fft=512, hop_length=64)
features = np.vstack((features, contrast)).T
recognized = []

variations = get_variations(model.predict(features))
print(variations)
variations = [compact(item) for item in variations]
for var in variations:
    st = [phonem_list[ph] for ph in var]
    print("===================================")

    print(st)
print("===================================")
print("real one", words['days'])

sys
for feature in features.T:

    prediction = model.predict(np.array([feature]))
    prediction_buffer.append(prediction)
    ++index2

    index = np.argmax(prediction)
    indices = []
    prob = []
    pr = ""
    for i in range(4):
        x = np.argmax(prediction[0])
        indices.append(x)
        prob.append(prediction[0][x])
        prediction[0][x] = 0
        pr += str(prob[i]) + ": " + str(phonem_list[indices[i]]) + ", "
    print(pr)
    if(prediction[0][index] > 0.1):
        recognized.append(phonem_list[index])
        


print(words['thirteen'])
print(word_corrector(prediction_buffer,recognized,phonem_list)[0])
'''print(word_corrector(prediction_buffer,recognized))'''
#print(words['thirteen'])
print(recognized)
#print(type(recognized))
compacted_recognized = compact(recognized)
print(compacted_recognized)

