import datetime
import xml.etree.ElementTree as ET
from os import listdir
from os.path import isfile, join, isdir
import os.path
from os import path
import librosa
import soundfile as sf
import json
import numpy as np

words = {}
sounds = []
soundCount = 0
distinct_words = {}

phonems = set() # We save the set of phonems in this set
with open('distinct_sounds.json') as words_file:
    data = json.load(words_file)
    for phonem in data:
        phonems.add(phonem)
phonems.add("None") # We add the phonem None
phonem_list = list(phonems) # We list the phonems
phonem_list.sort() # We sort the phonems
print(phonems)
phonems_count = len(phonems) #Count the number of phonems that we have
samplerate = 0
mfcc_window_size = 512
mfcc_hop_size = 128

training_data = np.zeros((0, phonems_count + 20)) #??
#For all the articles in english
articles = [f for f in listdir('english') if isdir(join('english', f))]
article_index = 0
articles.sort() # We sort all the articles
currentDT_prev = datetime.datetime.now()
for article in articles:
    article_index += 1 # we get the number of article found
    print("Found article ", article, " (", article_index, "/", len(articles), ")")
    article_path = join('english', article)
    filename = join(article_path, 'aligned.swc') # We access to the swc file
    
    if not path.exists(filename):
        continue # if it doesn't have swc we ignore
    root = ET.parse(filename).getroot()

    recordings = []
    recording_offsets = []
    audio_files = [f for f in listdir(article_path) if isfile(join(article_path, f)) and f.endswith(".ogg")]
    audio_files.sort()
    try:
        for audio_file in audio_files:
            data, samplerate = librosa.load(join(article_path, audio_file))
            print("Sample rate", samplerate)
            print("Samples", len(data))
            print("Duration", len(data)/samplerate, "s")
            recording = {}
            recording['data'] = data
            recording['samplerate'] = samplerate
            recording['duration'] = len(data)/samplerate
            recording['features'] = librosa.feature.mfcc(y=data, sr=samplerate, n_fft=mfcc_window_size, hop_length=mfcc_hop_size).T
            recordings.append(recording)

    except:
        print("Cant read audio file")
        continue
    
    for audio in root.findall(".//prop[@key='DC.source.audio.offset']"):
        print(audio.attrib["value"], audio.attrib["group"])
        recording_offsets.append(float(audio.attrib["value"]))

    all_nodes = root.findall(".//ph")
    print("This article has", len(all_nodes), "phonems annotated")
    for ph in all_nodes:
        
        sound = {}
        s = ph.attrib["type"]
        sound['sound'] = s
        soundCount = soundCount + 1

        recording_index = 0
        for offset in recording_offsets:
            if float(ph.attrib['start']) > offset:
                break
            recording_index = recording_index + 1
        #print("Offset", sound['start'], "is in recording", recording_index)
        start_in_file = float(ph.attrib['start']) - recording_offsets[recording_index]
        end_in_file = float(ph.attrib['end']) - recording_offsets[recording_index]
        starting_sample = int(start_in_file * recordings[recording_index]['samplerate']/1000)
        ending_sample = int(end_in_file * recordings[recording_index]['samplerate']/1000)

        sound['start'] = starting_sample
        sound['end'] = ending_sample

        sounds.append(sound)

        #phonem_index = phonem_list.index(s)
        #output = np.zeros((phonems_count))
        #output[phonem_index] = 1.0
        #con = np.concatenate((output, features.flatten()))
        #training_data = np.vstack((training_data, [con]))
        
        #print(training_data.shape)

    sounds = sorted(sounds, key=lambda k: k['start']) 
    recording_index = 0
    for recording in recordings:
        features = recording['features']
        print("we have ", len(features), "features")
        print("Recording offset is", recording_offsets[recording_index])
        feature_start = int(recording_offsets[recording_index] * recordings[recording_index]['samplerate'] / 1000)

        for feature in features:
            feature_end = feature_start + mfcc_window_size
            durations = np.zeros(phonems_count)
            durations_sum = 0
            #print("feature start", feature_start, "feature end", feature_end)
            for phonem in sounds:
                if (feature_end > phonem['start'] and feature_start < phonem['start']) or (feature_end > phonem['end'] and feature_start < phonem['end']):
                    #print("In")
                    phonem_index = phonem_list.index(phonem['sound'])
                    new_duration = min(feature_end, phonem['end']) - max(feature_start, phonem['start'])
                    durations[phonem_index] += new_duration
                    durations_sum += new_duration
                    
                if feature_start > phonem['end']:
                    break

            if durations_sum > 0:
                if durations_sum < mfcc_window_size:
                    phonem_index = phonem_list.index("None")
                    durations[phonem_index] += mfcc_window_size - durations_sum
                durations = durations / durations.sum()
                #print(durations)
                con = np.concatenate((durations, feature.flatten()))
                training_data = np.vstack((training_data, [con]))

            sounds = [d for d in sounds if d['end'] > feature_start]
            if len(sounds) == 0:
                break

            #sounds = filter(lambda x: feature_start > x['end'], sounds)
            feature_start += mfcc_hop_size
        recording_index = recording_index + 1
    np.savez_compressed("training_data.npz", training_data)


#
currentDT_post = datetime.datetime.now()
print("execution time:",currentDT_post - currentDT_prev)
print(training_data)

np.savez_compressed("training_data.npz", training_data)

print("sounds ", soundCount)

