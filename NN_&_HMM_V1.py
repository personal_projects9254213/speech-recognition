# -*- coding: utf-8 -*-
"""
Created on Thu Feb 20 15:48:44 2020

@author: Miguel
"""
import numpy as numpy
import enum

class phonemm(enum.Enum):
    AA = 0
    AE = 1
    AH = 2
    AO = 3
    AW = 4
    AY = 5
    B = 6
    CH = 7
    D = 8
    DH = 9
    EH = 10
    ER = 11
    EY = 12
    F = 13
    G = 14
    HH = 15
    IH = 16
    IY = 17
    JH = 18
    K = 19
    L = 20
    M = 21
    N = 22
    NG = 23
    OW = 24
    OY = 25
    P = 26
    R = 27
    S = 28
    SH = 29
    T = 30
    TH = 31
    UH = 32
    UW = 33
    V = 34
    W = 35
    Y = 36
    Z = 37
    ZH = 38
    END = 39
    

'''
beggining of neural network's code
'''

numpy.random.seed(42)
#softmax converts the imput values to a range between 0 and 1 being the sum
#of them equal to 1 and the converted values proportionals to the originals
def softmax(x):
    xx = numpy.exp(x - numpy.max(x))
    return xx / xx.sum()

def sigmoid(x):
    return 1/(1+numpy.exp(-x))

def sigmoid_der(x):
    return sigmoid(x) *(1-sigmoid (x))



input = [1, 2,4,5]
numInput = 36
numNeuronsHidden = 36
numOutput = 27
lr = 10e-4


#each row is a training example and each column a neuron
'''
introduce in the next line a matrix including the generated
inputs for training the Neural Network, in the following one
a 1 in the correct expected sound and 0 in the rest of the positions
(one position per neuron(one neuron per possible sound))
'''
trainInput = numpy.array([[]])
trainOutput = numpy.array([[]])
   
   
#creation of the synapses matrices
syna1 = 2 * numpy.random.random((numInput,numNeuronsHidden)) - 1
syna2 = 2 * numpy.random.random((numNeuronsHidden,numOutput)) - 1
   
   
#training loop
for j in range(1,1000):
    layer1 = trainInput
    op1 = numpy.dot(layer1,syna1)
    layer2 = sigmoid(op1)
    op2 = numpy.dot(layer2,syna2)
    layer3 = softmax(op2)#layer 3 is the output
       
    '''In our current design (which works with probabilities as outputs we will use the probabilistic
       value of the synapses as the HMM's input'''
    #apply to the third layer the HMM and combine the probabilities in both getting the solution phonem
    
    '''we modificate the synapses values according to the obtained result
       this is done raising the probability of our real phonem and decreasing all the others
       depending on the position the real value had in the probability list of the neural network
       (the magnitude of the error) the values will be modified in a greater or smoother way'''
    derl3cost = layer3 - trainOutput
    auxl2 = layer2#unsure of the need of an auxiliar var
    newSyna2 = numpy.dot(auxl2.T, derl3cost)
    auxderl3 = derl3cost#unsure of the need of an auxiliar var
    
    syna2aux = syna2#unsure of the need of an auxiliar var
    dcostdl2 = numpy.dot(derl3cost, syna2aux.T)
    dl2dl3 = sigmoid_der(op1)
    inputAux = trainInput#unsure of the need of an auxiliar var
    newSyna1 = numpy.dot(inputAux.T, dl2dl3 * dcostdl2)
       
    syna1 -= lr * newSyna1
    syna2 -= lr * newSyna2

'''
beggining of HMM's code
'''
phonems = []#the phonems we already considered as good for the current word
likelyhood = numpy.array([0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0])#list with the size of the number of existent phonems which contains their likelyhood for the HMM

#we create a list of tuples which in order will contain word, phonems' list and likelyhood for our dictionary
dictionary = []
#list of words in the dictionary which share the same starting phonems as the currently reading word
comparable = []
i = 0
best = 0
aux = 0
valid = True
totalLikelyhood = 0
'''
check for probability of the words that start with our initial phonems
creating a list of the most likely next phonem in case neural network's
reliability is not high enought
'''
if len(phonems) != 0:
    likelyhood = likelyhood*0
    if len(phonems) == 1:
        for word in dictionary:
            i = 0
            for phonem in phonems:
                if word[i] != phonem:
                    valid = False
                    break
                ++i
            if valid == True:
                likelyhood[word[1][i]] += word[2]#important to notice that end of word is also a phonem
                comparable.append(word)
            else:
                valid = True
    else:
        for word in comparable:
            i = 0
            for phonem in phonems:
                if word[i] != phonem:
                    valid = False
                    break
                ++i
            if valid == True:
                likelyhood[word[1][i]] += word[2]#important to notice that end of word is also a phonem
            else:
                valid = True
                comparable.remove(word)
    #we get the real probability of each phonem
    totalLikelyhood = numpy.sum(likelyhood)
    likelyhood = likelyhood/totalLikelyhood
    k = 0
    for pho in layer3:
        layer3[k] = likelyhood[k]*layer3[k]#this equation defines the relationship between the NN and the HMM multiplying both guesses
        ++k

k = 0
for pho2 in layer3:
    if pho2 > aux:
        aux = pho
        best = k
    ++k
phonems.append(best)


