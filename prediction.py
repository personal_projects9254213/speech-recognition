from keras.models import Sequential
from keras.layers import Dense
import json
import numpy as np
from keras.models import load_model
import librosa

def compact(recognized):
    compacted_recognized = []
    compacted_recognized.append(recognized[0])

    for i in range(1, len(recognized)):
        #print(recognized[i])
        if recognized[i] != recognized[i-1]:
            compacted_recognized.append(recognized[i])
    return compacted_recognized

'''
This function receives an array with the last outputs of the neural networks
should be called every space so that it checks the possible words erasing those spaces 
and the likelyhood of each of those(applied with the original chain, substitutes HMM's correction
in case it has a modifying output)
IMPORTANT: the input must be a chain of not recognised phonems and spaces
'''
def space_prediction(chain):
    with open('words.json') as json_file:
        words = json.load(json_file)
        wordss = [[]]
        wordd = ""
        ind = 0
        for phon in chain:
            if phon != ' ':#substitute with the phonem representation of space
                wordss[ind].append(phon)
            else:
                ++ind
        for j in range(1,ind+1):#for each number of consecutive erased spaces
            for k in range(0,ind+1-j):
                for s in range(0,j):
                    wordd += wordss[k+s]
                for word in words:
                    if wordd in word:
                        #combination found
                        #giving chain the shape of its corresponding words now
                        chain = ""
                        for w in range(0,k):
                            chain += wordss[w]
                            chain += ' '
                        chain +=wordd
                        for w in range(k+s+1,ind+1):
                            chain += ' '
                            chain += wordss[w]
                        return
                wordd = ""

'''
levenshtein distance
'''
def lev_distance(chain1, chain2):
  d=dict()
  for i in range(len(chain1)+1):
     d[i]=dict()
     d[i][0]=i
  for i in range(len(chain2)+1):
     d[0][i] = i
  for i in range(1, len(chain1)+1):
     for j in range(1, len(chain2)+1):
        d[i][j] = min(d[i][j-1]+1, d[i-1][j]+1, d[i-1][j-1]+(not chain1[i-1] == chain2[j-1]))
  return d[len(chain1)][len(chain2)]
        
'''
array of equidstant words
'''
def lev_equi(chain):
    with open('words.json') as json_file:
        output = {}
        ext = 0
        words = json.load(json_file)
        while(not output):
            for word in words:
                for pronounciation in words[word]:
                    if lev_distance(pronounciation,chain) == ext:
                        output.append(word)
                        break
            ++ext
        return output


'''
gets the most likely out of the equidistant words

def word_corrector(prediction_buffer,recognized):
    index = 0
    arrayy = lev_equi(recognized)
    for i in range(0,5):#levels to go down in the possible phonems schale before checking on adding a letter
        for value in array:
            for phonn in value:
                if(phonn != recognized):
        
'''

print(np.array([1, 2, 3]).shape)

words = {}
phonems = set()
prediction_buffer = []
index2 = 0
with open('words.json') as words_file:
    data = json.load(words_file)
    for word in data:
        words[word] = data[word][0]
        for phonem in words[word]:
            phonems.add(phonem)
phonem_list = list(phonems)
phonem_list.sort()
print(phonems)
phonems_count = len(phonems)

model = load_model('model.h5')
data, samplerate = librosa.load('words/thirteen.wav', sr=None)
interval_ms = 50
interval_samples = int(interval_ms * samplerate / 1000)
print(interval_ms, "ms at ", samplerate, "samples/s equals to ", interval_samples, "samples")
features = librosa.feature.mfcc(y=data, sr=samplerate, hop_length = int(interval_samples/3), n_fft=interval_samples)
recognized = []
for feature in features.T:
    prediction = model.predict(np.array([feature]))
    prediction_buffer.append(list.copy(prediction))
    ++index2
    index = np.argmax(prediction)
    if(prediction[0][index] > 0.0):
        recognized.append(phonem_list[index])
        print(prediction[0][index], phonem_list[index])
'''print(word_corrector(prediction_buffer,recognized))'''
print(words['thirteen'])
print(recognized)
#print(type(recognized))
compacted_recognized = compact(recognized)
print(compacted_recognized)

