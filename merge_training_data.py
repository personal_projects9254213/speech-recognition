from os import listdir
from os.path import isfile, join, isdir
import json
import numpy as np

phonems = set()
with open('distinct_sounds.json') as words_file:
    data = json.load(words_file)
    for phonem in data:
        phonems.add(phonem)
phonems.add("None")
phonem_list = list(phonems)
phonem_list.sort()
print(phonems)
phonems_count = len(phonems)

training_data = np.zeros((0, phonems_count + 20))

files = [f for f in listdir('training_data') if isfile(join('training_data', f))]
for file in files:
    filename = join('training_data', file)
    print("Adding file", filename)
    td = np.load(filename)['arr_0']
    training_data = np.vstack((training_data, td))

np.savez_compressed("training_data.npz", training_data)
