# -*- coding: utf-8 -*-
"""
Created on Thu Mar  5 16:05:06 2020

@author: Lytvynets
"""

import tensorflow as tf
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Dropout, LSTM 
import json
import numpy as np
import pandas as pd

with open('sounds.json') as json_file:
    data = json.load(json_file)

    phonems = set()
    cases = 0
    for phonem in data:
        phonems.add(phonem)
        cases += len(data[phonem])
    phonem_list = list(phonems)
    phonem_list.sort()
    print(phonems)
    phonems_count = len(phonems)
    print("Cases:", cases)

    X = np.zeros((cases, 20))
    y = np.zeros((cases, phonems_count))#this one
    index = 0
    for phonem in data:
        for v in data[phonem]:
            if len(v) == 20:
                X[index] = v
                y[index][phonem_list.index(phonem)] = 1
                index = index + 1
    print(X)
    print(y)
    
'''
this lines of code create a new np.array which cuts the sting of data inputs into pieces of size 4 samples
being this the window size used for the LSTM
'''
X2 = np.zeros((cases, 4, 20))
for i in range(0,cases):
    for j in range(0,4):
        if(i-j < 0):
            X2[i][j] = X[0]
        else:
            X2[i][j] = X[i-j]


    
'''    
lahead = 4#lahead is the window size for the LSTM check
if lahead > 1:
    X = np.repeat(X, repeats=lahead, axis=1)
    X = pd.DataFrame(X)
    print(X)
    for i, c in enumerate(X.columns):
        X[c] = X[c].shift(i)
'''
model = Sequential()
'''lahead,'''
model.add(LSTM(80, input_shape=(4,20), activation ='relu', return_sequences= True)) # return_sequences is used for when you're continuing on to another recurrent layer
model.add (Dropout(0.2))


model.add(LSTM(80,activation ='relu'))
model.add (Dropout(0.2))

model.add(Dense(60, activation='relu'))
model.add (Dropout(0.2))

model.add(Dense(phonems_count, activation='softmax'))


opt= tf.keras.optimizers.Adam(lr=1e-3, decay=1e-6)

model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])
model.fit(X2, y, epochs=50, batch_size=10, validation_data=(X2, y))#not ok in validation data
model.save('model.h10')

#model.fit(x_train, y_train, epochs=3, validation_data=(x_test, y_test))

    

    