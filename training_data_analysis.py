from keras.models import Sequential
from keras.layers import Dense
import json
import numpy as np

phonems = set()
with open('distinct_sounds.json') as words_file:
    data = json.load(words_file)
    for phonem in data:
        phonems.add(phonem)
phonems.add("None")
phonem_list = list(phonems)
phonem_list.sort()
print(phonems)
phonems_count = len(phonems)

training_data = np.load("training_data_512.npz")['arr_0']
x = training_data[:, phonems_count:]
y = training_data[:, 0:phonems_count]

print(x)
print(y)
pure = np.zeros(phonems_count)
large = np.zeros(phonems_count)
mm = np.zeros(phonems_count)
sums = np.zeros(phonems_count)
for case in y:
    idx = 0
    for prob in case:
        if prob == 1.0:
            pure[idx] += 1
        if prob > 0.9:
            large[idx] += 1
        if prob > 0.7:
            mm[idx] += 1
        sums[idx] += prob
        idx += 1

for c in range(phonems_count):
    print(phonem_list[c], pure[c], large[c], mm[c], int(sums[c]))
