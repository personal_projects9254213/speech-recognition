
import json
import numpy as np

with open('words.json') as json_file:
    words = json.load(json_file)
    phonems = set()
    for word in words:
        for pronounciation in words[word]:
            for ph in pronounciation:
                phonems.add(ph)
    phonem_list = list(phonems)
    phonem_list.sort()
    print(phonems)
    phonems_count = len(phonems)
    model = np.zeros((phonems_count, phonems_count))

    for word in words:
        for pronounciation in words[word]:
            for i in range(1, len(pronounciation)):
                previous = phonem_list.index(pronounciation[i-1])
                current = phonem_list.index(pronounciation[i])
                model[previous][current] += 1
    row_sums = model.sum(axis=1)
    model = model / row_sums[:, np.newaxis]
    print(model)

    
